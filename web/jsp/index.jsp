<%-- 
    Document   : index
    Created on : 3 jun. 2021, 8:34:09
    Author     : madarme
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="../css/index.css">
        <title>Primer JSP</title>
    </head>
    <body>
        <div id="banner">
            <p class="titulo"> Ejemplo MVC</p>
        </div>

        <div id="contenido">
            <h1>Ejemplo de JSP-Directivas</h1>

            <%
                String mensajes = "numero ";
                for (int i = 0; i < 10; i++) {
                    mensajes = "<p>" + mensajes + i + "</p>";
                }

            %>


            <h2>
                
                <%=mensajes%>
            </h2>
            <div class="clear"></div> 
        </div>



        <div id="pie">
            Copyright © 2021
            Autor: MARCO A. ADARME
            |   Cod: 04041
            |   Correo: madarme@ufps.edu.co
            <br/>
            Programación Web 
            <a href="http://ingsistemas.ufps.edu.co/" target="_blank">Ing.Sistemas</a> - 
            <a target="_blank" href="http://ufps.edu.co/">UFPS</a>
        </div>


    </body>
</html>
