/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Modelo.Numero;

/**
 *
 * @author madar
 */
public class Calculadora {

    
    private Numero num_1, num_2;
    
    public Calculadora() {
    }

    
    
     
    
    public int getSuma(String a, String b) {
        int num1 = Integer.parseInt(a);
        int num2 = Integer.parseInt(b);
        this.num_1 = new Numero(num1);
        this.num_2 = new Numero(num2);
        return num_1.getDato() + num_2.getDato();
    }

    public Numero getNum_1() {
        return num_1;
    }

    public void setNum_1(Numero num_1) {
        this.num_1 = num_1;
    }

    public Numero getNum_2() {
        return num_2;
    }

    public void setNum_2(Numero num_2) {
        this.num_2 = num_2;
    }

    
   public String getNumeros() 
   {
       return "Los numeros son:"+this.num_1+","+this.num_2;
   }
    
}
