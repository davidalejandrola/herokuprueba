/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author madar
 */
public class Numero {
    
    private int dato;

    public Numero() {
    }

    public int getDato() {
        return dato;
    }

    public void setDato(int dato) {
        this.dato = dato;
    }

    
    public Numero(int dato) {
        this.dato = dato;
    }
    
    @Override
    public String toString() {
        return "Numero{" + "dato=" + dato + '}';
    }
    
    
    
    
}
